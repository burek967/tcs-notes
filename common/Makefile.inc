PANDOC_BIN:=pandoc

# LaTeX settings.
LATEX_TEMPLATE := ../../templates/template.latex
LATEX_OUTPUT := out.pdf
LATEX_FLAGS = --template ${LATEX_TEMPLATE} --toc --listings

# HTML settings.
HTML_TEMPLATE := ../../templates/template.html5
HTML_OUTPUT := out.html
HTML_FLAGS = --template ${HTML_TEMPLATE} --toc --katex

# Rules.
define BUILD_RULES =
all: latex
latex:
	${PANDOC_BIN} ${LATEX_FLAGS} -o ${LATEX_OUTPUT} ${SOURCES}
html:
	${PANDOC_BIN} ${HTML_FLAGS} -o ${HTML_OUTPUT} ${SOURCES}
endef
