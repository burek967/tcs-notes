TCS@Jagiellonian Lecture Notes
==============================

This repository contains as much information from lectures as we could
accumulate. If you find the contents helpful and would like to contribute,
please contact burek.

Also please keep in mind that authors are not infallible and some statements
may be not precise enough or simply false. In this case also please report it
in any way possible. Thank you from the mountain.
